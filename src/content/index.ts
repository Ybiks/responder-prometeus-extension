import * as load from 'load-script';

import config from './config';
import logger from '@/libs/logger';

window.onerror = (event, source, fileno, columnNumber, error) => logger.sendCritical(error);

if (config.supportedDomains.some(domain => document.location.href.match(domain))) {
    load(chrome.extension.getURL('/js/app.js'), error => {
        if (error) {
            logger.sendCritical(error);
        }
    });
}
