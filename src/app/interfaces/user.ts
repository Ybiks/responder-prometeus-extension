import { IRight } from './rights';
import { University } from './university';

export enum Gender {
    male = 'male',
    female = 'female',
}

export type rightsOfUser = IRight[];

export enum UserControlStrategies {
    auto = 'auto',
    manual = 'manual'
}

export interface IUserControl {
    accuracy: number;
    strategy: UserControlStrategies;
    keysModal: number[];
    showAnswer: number[];
    keysHide: number[];
}

export interface IUserSettings {
    control: IUserControl;
    quickSignIn: boolean;
}

export interface IUserConfirmations {
    idPrometeus: boolean;
    email: boolean;
}

export interface IUserPrivateData {
    nickname: string;
    service: string;
    ref: string;
    settings: IUserSettings;
    createdAt: string;
    gender: Gender;
    numberOfAnswers: number;
    confirmations: IUserConfirmations;
    let: string;
    avatar: string;
    rights: rightsOfUser;
}