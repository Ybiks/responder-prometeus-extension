export enum Protocol {
    https = 'https:',
    http = 'http:'
}

export enum University {
    bukep = 'cdo.bukep.ru',
}