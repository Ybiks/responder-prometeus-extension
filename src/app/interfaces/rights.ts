export enum Category {
    news = 'news'
}

export enum RightTo {
    read = 'read',
    edit = 'edit',
    full = 'full',
}

export interface IRight {
    category: Category;
    actions: RightTo;
};