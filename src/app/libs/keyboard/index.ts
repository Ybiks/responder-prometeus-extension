import { Store } from 'redux-act';

import { IRootState } from '@/store/interface';
import KeyboardEvent from '@/libs/keyboard/Event';
import {
	getToken,
	getStrategy,
	getKeysHide,
	getKeysModal,
	getKeysShowAnswer,
	getDocumentsFromTargets
} from '@/store/selectors';
import {
    modalActions,
} from '@/store/modules/modal/actions';
import { commonActions } from '@/store/modules/common/actions';
import { UserControlStrategies } from '@/interfaces/user';

//Массив ссылк на бинды, необходим для их снятия
let bindes = [];

export default (store: Store<IRootState>) => {
    const state = store.getState();
	bindes.forEach(bind => bind.unbind()); //Все устаревшие бинды снимаем

	let allTargets: (HTMLElement|Document)[] = [
		document,
		document.getElementById('topFrame'),
		document.getElementById('leftFrame'),
		...getDocumentsFromTargets(state)
    ];

	//Присваиваем новый массив c установленными обработчиками
	bindes = [].concat(
		//Изменение отображения модального окна
		new KeyboardEvent(allTargets, getKeysModal(state), () => store.dispatch(modalActions.autoChangeDisplayModal())),
        
        //Выключить работу плагина
		new KeyboardEvent(allTargets, getKeysHide(state), () => {
			//
		})
	);

	if (getToken(state) && getStrategy(state) === UserControlStrategies.manual) {
		//Подсвет ответа
        bindes.push(
            new KeyboardEvent(allTargets, getKeysShowAnswer(state), () => store.dispatch(commonActions.eventSetAnswer()))
        );
    }
};