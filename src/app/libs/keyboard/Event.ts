export default class {
	constructor(targets: (HTMLElement|Document)[], keys, callback){
		this.targets = targets;
		this.callback = callback;

		keys.forEach((key) => {
			this.keys[key] = false;
		});

		this.keyupBinds = this.keyup.bind(this);
		this.keydownBinds = this.keydown.bind(this);

		this.targets.forEach(target => {
            target.addEventListener('keyup', this.keyupBinds);
			target.addEventListener('keydown', this.keydownBinds);
		});
    }
    
    keys: object = {};
    targets: (HTMLElement|Document)[];
    callback: (e: KeyboardEvent) => void;
    keyupBinds: (e: KeyboardEvent) => void;
    keydownBinds: (e: KeyboardEvent) => void;

	setCallback(callback){
		this.callback = callback;
	}

	keydown(event: KeyboardEvent){
		if (this.keys.hasOwnProperty(event.keyCode)) {
			this.keys[event.keyCode] = true;

			if(Object.keys(this.keys).every((key) => {
				return this.keys[key];
			})){
				Object.keys(this.keys).forEach((key) => {
					this.keys[key] = false;
				});
				this.callback(event);
			}	
		}
	}

	keyup(event: KeyboardEvent){
		if (this.keys.hasOwnProperty(event.keyCode)) {
			this.keys[event.keyCode] = false;
        }
	}

	unbind(){
		this.targets.forEach(target => {
            target.removeEventListener('keyup', this.keyupBinds);
            target.removeEventListener('keydown', this.keydownBinds);	
		});
	}
}
