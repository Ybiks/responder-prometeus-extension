import { ITarget } from "@/store/modules/targets/interface";

export default (callback: (targets: ITarget[]) => void) => {
	let targets = [];
	setInterval(() => {
		let currentTargets: ITarget[] = [];

		function findFormElements(frame: HTMLFrameElement) {
			if(!frame) return;
			const document = frame.contentWindow.document;
			currentTargets.push({
				frame,
				document
			});

			const frames = document.getElementsByTagName('frame');
			if(frames.length > 0)
				Array.prototype.slice.call(frames).forEach(el => findFormElements(el));
			

			const iframes = document.getElementsByTagName('iframe');
			if(iframes.length > 0)
				Array.prototype.slice.call(iframes).forEach(el => findFormElements(el));
		}
		
		findFormElements(document.getElementById('mainFrame') as HTMLFrameElement);

		if (!currentTargets.every(target => target.document.myCustomField)) {

			targets = currentTargets.map(target => {
				target.document.myCustomField = true;
				return target;
            });

			callback(targets);
		}
	}, 50);
};