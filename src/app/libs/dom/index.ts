export { default as marker } from './marker';
export { default as initialWatcherForTargets } from './initialWatcherForTargets';

export const getFormsFromTarget = (target: Document): HTMLFormElement[] => 
	Array.prototype.slice.call(target.getElementsByTagName('form') || []);

export const checkCorrectInForm = (form: HTMLFormElement): boolean =>
	(form.getAttribute('name') === 'frm') && (form.getElementsByTagName('tbody').length > 0);

export const getIdQuestion = (form: HTMLFormElement): string|null => {
    const element = form.querySelector<HTMLInputElement>('input[name="idQuestion"]');

    return element ? element.value : null;
}