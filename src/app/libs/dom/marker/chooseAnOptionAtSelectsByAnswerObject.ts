import { IAnswerObject } from "@/api/interface";

// TODO: Added logger
export default (form: HTMLFormElement, answer: IAnswerObject) => {
    try {
        const selects = form.querySelectorAll('select');
        selects.forEach(select => {
            const tds = select.parentElement.parentElement.querySelectorAll('td');
            const first = tds && tds[0];
            const questionText = first && first.textContent;
    
            if(answer.question == questionText){
                const options = select.querySelectorAll('option');
                options.forEach(option => {
                    if (answer.correctAnswers.lastIndexOf(option.textContent) != -1) {
                        option.setAttribute('selected', 'selected');
                    } else {
                        option.removeAttribute('selected');
                    }
                });
            }
        });
    } catch (err) {
        console.log(err);
    }
};