// import registerError from '../../api/registerError';
import { IAnswerObject } from '@/api/interface';

export default (form: HTMLFormElement, answer: IAnswerObject) => {
    try {
        const fields = form.querySelectorAll('input[name=answ]');
    
        if(fields.length == 1, answer){
            if (answer.correctAnswers) {
                fields.forEach(field => {
                    field.setAttribute('value', answer.correctAnswers[0]);
                });
            }
        }else{
            /* registerError({
                id: 12,
                answer: answer,
                form: form.textContent,
                message: `Количество полей ввода ${fields.length}. Необходимо только 1`
            }); */
        }
    } catch (err) {
        console.log(err);
    }
};