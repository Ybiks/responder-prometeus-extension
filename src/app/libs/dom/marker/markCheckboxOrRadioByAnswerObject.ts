import { IAnswerObject } from '@/api/interface';

// import registerError from '../../api/registerError';
import { processingText } from '@/utils';

export default (form: HTMLFormElement, answer: IAnswerObject) => {
    try {
        const inputs = form.querySelectorAll('tbody input[name=answ]');
        let answered = 0;
    
        inputs.forEach((input: HTMLInputElement) => {
            let currentVariant;
            const tr = input.parentElement.parentElement;
            const tds = tr.getElementsByTagName('td');
    
            const labelText = tds.item(2).getElementsByTagName('label').item(0).textContent;
            const correctLabelText = labelText && labelText.replace(/([\n]+)/, '');
        
            const img = tds.item(1).getElementsByTagName('img').item(0);
            const imagePath = img && img.getAttribute('src');
    
            currentVariant = processingText(imagePath ? imagePath : correctLabelText);
    
            if(answer.correctAnswers.lastIndexOf(currentVariant) != -1){
                input.checked = true;
                answered++;
            }else{
                input.checked = false;
            }
            
        });
    
        if(answered != answer.correctAnswers.length) {}
            /* registerError({
                id: 11,
                answer: answer,
                form: form.textContent,
                message: 'Не все ответы удалось пометить'
            }); */

    } catch(err) {
        console.log(err);
    } 
};