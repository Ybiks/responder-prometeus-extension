import { IAnswerObject } from '@/api/interface';

import chooseAnOptionAtSelectsByAnswerObject from './chooseAnOptionAtSelectsByAnswerObject';
import markCheckboxOrRadioByAnswerObject from './markCheckboxOrRadioByAnswerObject';
import fillInFieldByAnswerObject from './fillInFieldByAnswerObject';
import sortOrderByAnswerObject from './sortOrderByAnswerObject';

export default async (form: HTMLFormElement, answers: IAnswerObject[]) => {
	answers.forEach( answer => {
		if(answer.type == 'match'){
			chooseAnOptionAtSelectsByAnswerObject(form, answer);
		}else if(answer.type == 'checkbox' || answer.type == 'radio'){
			markCheckboxOrRadioByAnswerObject(form, answer);
		}else if(answer.type == 'field'){
			fillInFieldByAnswerObject(form, answer);
		}else if(answer.type == 'order'){
			sortOrderByAnswerObject(form, answer);
		}
	});
};