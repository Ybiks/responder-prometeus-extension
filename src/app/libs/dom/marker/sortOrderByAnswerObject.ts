import { IAnswerObject } from '@/api/interface';
import { processingText } from '@/utils';

export default (form: HTMLFormElement, answer: IAnswerObject) => {
    try {
        const ul = form.querySelector('ul');
        const lis = ul.querySelectorAll('li');
        const sequence = [];
    
        answer.correctAnswers.forEach(an => {
            const seq = an.match(/([^\(\)]+)/g);
            sequence[Number(seq[0])] = seq[1];
        });

        ul.textContent = '';
        
        sequence.forEach((variant: string) => {
            lis.forEach(li => {
                const tds = li.querySelectorAll('td');
                const lastTd = tds && tds.item(tds.length - 1);
                const text = lastTd && lastTd.textContent;

                if (variant == processingText(text)) {
                    ul.appendChild(li);
                }
            })
        });
    } catch (error) {
        console.log(error);
    }
};