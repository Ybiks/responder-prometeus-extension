import * as Raven from 'raven-js';

import config from '@/config';

try {
    Raven.config(config.ravenURL).install();
} catch (err) {
    console.error('Raven connect error:', err);
}

const wrap = (options: Raven.RavenOptions, callback: Function) => Raven.context(options, callback);
const setTagsMeta = (data: object) => Raven.setTagsContext(data);
const setUserMeta = (data: object) => Raven.setUserContext(data);

const sendWarn = (error: Error) => Raven.context({ level: 'warn' }, () => { throw error });
const sendError = (error: Error) => Raven.context({ level: 'error' }, () => { throw error });
const sendCritical = (error: Error) => Raven.context({ level: 'critical' }, () => { throw error });

export default {
    wrap,
    setTagsMeta,
    setUserMeta,

    sendWarn,
    sendError,
    sendCritical,
}