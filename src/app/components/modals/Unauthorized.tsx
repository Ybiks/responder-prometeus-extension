import * as React from 'react';
import ModalComponent from './ModalComponent';
import { Container } from '../ui-kit/Container';

export default ({ visible }) => (
	<ModalComponent visible={visible} title='Ошибка сценария'>
        <Container padding='20px 10% 20px 10%'>
		    Для нормальной работы расширения, вы должны быть авторизованны на сайте.
        </Container>
	</ModalComponent>
)