import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux-act';

import { IRootState } from '@/store/interface';
import { modalActions } from '@/store/modules/modal/actions';
import { getIsShowModal, getIdPrometeus, getToken, getProfile, getVerificationCode } from '@/store/selectors';

import SignIn from './SignIn';
import Unauthorized from './Unauthorized';
import Profile from './Profile';
import logger from '@/libs/logger';
import VerificationCode from './VerificationCode';

interface IProps {
    state: IRootState;
}

interface IState {
    hasError: boolean;
}

class RouterModal extends React.Component<IProps, IState> {
    state = {
        hasError: false,
    }

    componentDidCatch(error) {
        logger.sendError(error);
        this.setState({ hasError: true });
    }

    render () {
        const { state } = this.props;
        const { hasError } = this.state;
        const visible = getIsShowModal(state);
    
        if (hasError) return null;
    
        const idPrometeus = getIdPrometeus(state);
        if (!idPrometeus) {
            return <Unauthorized visible={visible} />;
        }

        const verificationCode = getVerificationCode(state);
        if (verificationCode) {
            return <VerificationCode verificationCode={verificationCode} visible={visible} />
        }
    
        const token = getToken(state);
        if (!token) {
            return <SignIn visible={visible} />
        }
    
        const profile = getProfile(state);
        if (profile) {
            return <Profile visible={visible} />
        }

        return null;
    }
};

const mapStateToProps = (state: IRootState) => ({
    state,
})

export default connect(mapStateToProps)(RouterModal);
