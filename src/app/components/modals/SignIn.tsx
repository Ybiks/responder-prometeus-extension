import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Formik, FormikActions } from 'formik';

import ModalComponent from './ModalComponent';

import { University } from '@/interfaces/university';
import { signIn } from '@/api';

import { Link } from '../ui-kit/Link';
import { Button } from '../ui-kit/Button';
import { Container } from '../ui-kit/Container';
import { Input, Warning } from '../ui-kit/Input';
import { getIdPrometeusHash } from '@/utils';
import { profileActions } from '@/store/modules/profile/actions';
import { IRootState } from '@/store/interface';
import { getUniversity, getIdPrometeus } from '@/store/selectors';
import { Dispatch } from 'redux-act';
import { IProfileState } from '@/store/modules/profile/interface';
import { ITokenState } from '@/store/modules/token/interface';
import { tokenActions } from '@/store/modules/token/actions';
import logger from '@/libs/logger';

const SignInButton = styled(Button)`
    padding: 10px 20px;
    color: #fff;
    float: right;
`;

interface IFields {
    university: University;
    idPrometeus: string;
    password: string;
}

const getFormContent = ({
    values,
    errors,
    touched,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
}) => {
    return (
        <Container align='center' padding='20px 10% 20px 10%'>
            <form action='#' onSubmit={ handleSubmit }>
                <Input
                    type='text'
                    disabled={true}
                    value={values.idPrometeus}
                />
                <Input
                    type='password'
                    name='password'
                    onBlur={handleBlur}
                    placeholder='Пароль'
                    onChange={handleChange}
                    value={values.password}
                    error={touched.password && errors.password}
                />
                {
                    touched.password && errors.password && <Warning>{errors.password}</Warning>
                }
                <Container vertical='bottom' padding='0 0 10px 0'>
                    {/* <Link href='/df'>
                        Регистрация
                    </Link> */}
                    <SignInButton type='submit' disabled={isSubmitting}>Войти</SignInButton>
                </Container>
            </form>
        </Container>
    );
}

const closureOnSubmit = (setProfile, setToken) => ({ university, idPrometeus, password }: IFields, { setSubmitting, setErrors }: FormikActions<IFields>) => {
    setSubmitting(true);

    signIn({
        username: getIdPrometeusHash(idPrometeus),
        university,
        password,
    })
        .then(response => {
            setProfile(response.data.user);
            setToken(response.data.token);
            setSubmitting(false);
        })
        .catch(error => {
            setSubmitting(false);
            try {
                if (error.response.status === 401) {
                    setErrors({
                        password: 'Неверный пароль'
                    })
                    return;
                }
    
                if (error.response.status === 422) {
                    logger.sendWarn(error);
                    return;
                }

                throw error;
            } catch(cachedError) {
                logger.sendCritical(cachedError);
            }

        })
}

const checkValidate = (values: IFields) => {
    const errors: any = {};

    if (values.password.length < 6) {
        errors.password = 'Пароль не может быть меньше 6-ти символов';
    }

    return errors;
}

const SignIn = ({ visible, university, idPrometeus, setProfile, setToken }) => {
    const onSubmit = closureOnSubmit(setProfile, setToken);
    return (
        <ModalComponent visible={visible} title='Вход' reloadButton>
            <Container align='center'>
                <Formik
					onSubmit={ onSubmit }
					render={ getFormContent }
					validate={ checkValidate }
					initialValues={{ university, idPrometeus, password: '' } as IFields}
				/>
            </Container>
        </ModalComponent>
    )
}

const mapStateToProps = (state: IRootState) => ({
    university: getUniversity(state),
    idPrometeus: getIdPrometeus(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
    setProfile: (data: IProfileState) => dispatch(profileActions.setProfile(data)),
    setToken: (data: ITokenState) => dispatch(tokenActions.setToken(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)