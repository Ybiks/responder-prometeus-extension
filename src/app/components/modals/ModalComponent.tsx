import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux-act';

import { Modal, ModalTitle, ModalFullScreenButton, ModalCloseButton, ModalBody } from '../ui-kit/Modal';
import { modalActions } from '@/store/modules/modal/actions';
import { commonActions } from '@/store/modules/common/actions';

const mapDispatchToProps = (disbatch: Dispatch) => ({
    reload: () => disbatch(commonActions.initialApp()),
    hide: () => disbatch(modalActions.autoChangeDisplayModal())
});

type typeMapDispatchToProps = ReturnType<typeof mapDispatchToProps>;
type connectProps = typeMapDispatchToProps;

interface IProps extends connectProps {
    visible: boolean;
    resizeButton?: boolean;
    reloadButton?: boolean;
    title: string;
}

const ModalComponent: React.SFC<IProps> = ({ resizeButton, reloadButton, visible, children, title, reload, hide }) => (
    <Modal visible={visible}>
        <ModalTitle>
            {
                resizeButton &&
                <ModalFullScreenButton
                    src='https://flaticons.net/gd/makefg.php?i=icons/Application/Full-Screen-Expand.png&r=255&g=255&b=255'
                    onClick={hide}
                />
            }
            {
                reloadButton &&
                <ModalFullScreenButton
                    src='https://flaticons.net/gd/makefg.php?i=icons/Mobile%20Application/Command-Refresh.png&r=255&g=255&b=255'
                    onClick={reload}
                />
            }
            {title}
            <ModalCloseButton
                src='https://flaticons.net/gd/makefg.php?i=icons/Mobile%20Application/Close.png&r=255&g=255&b=255'
                onClick={hide}
            />
        </ModalTitle>
        <ModalBody>
            {children}
        </ModalBody>
    </Modal>
);

export default connect(null, mapDispatchToProps)(ModalComponent);