import * as React from 'react';
import styled from 'styled-components';

import ModalComponent from './ModalComponent';

import { Container } from '../ui-kit/Container';
import { Link } from '../ui-kit/Link';

const VerificationContainer = styled(Container)`
    padding: 39.5px 0;
    background-color: #4AA6DE;
`;

const CodeContainer = styled(Link)`
    font-size: 3.5rem;
    color: #fff;
    `;
    
const Description = styled.div`
    font-size: .85rem;
    color: #f5f5f5;
    padding-bottom: 15px;
`;

interface IProps {
    verificationCode: string;
    visible: boolean;
}

const VerificationCode = ({ verificationCode, visible }: IProps) => {
    return (
        <ModalComponent visible={visible} title='Регистрация' reloadButton>
            <VerificationContainer align='center'>
                <CodeContainer>{verificationCode}</CodeContainer>
                <Description>код подтверждения</Description>
            </VerificationContainer>
        </ModalComponent>
    )
}

export default VerificationCode;