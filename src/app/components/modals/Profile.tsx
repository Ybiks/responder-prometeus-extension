import * as React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import ModalComponent from './ModalComponent';

import { Container } from '../ui-kit/Container';
import { IRootState } from '@/store/interface';
import { getUniversity, getIdPrometeus, getProfile } from '@/store/selectors';
import { Dispatch } from 'redux-act';
import { IProfileState } from '@/store/modules/profile/interface';
import { commonActions } from '@/store/modules/common/actions';
import { Button } from '../ui-kit/Button';
import { Link } from '../ui-kit/Link';

const LogoutButton = styled(Button)`
    background-color: #fff;
    padding: 10px 20px;
    color: #333;
    margin: 11px 10% 11px 10%;
`;

const AnwsersContainer = styled(Container)`
    padding-top: 22px;
    background-color: #4AA6DE;
`;

const AnswersCounter = styled(Link)`
    font-size: 3.5rem;
    color: #fff;
    `;
    
const Description = styled.div`
    font-size: .85rem;
    color: #f5f5f5;
    margin-bottom: 15px;
`;

interface IProps {
    visible: boolean;
    profile: IProfileState;
    hide();
    logout();
}

const SignIn = ({ visible, profile, hide, logout }: IProps) => {
    return (
        <ModalComponent visible={visible} title='Профиль' hide={hide}>
            <AnwsersContainer align='center'>
                <AnswersCounter>{profile.numberOfAnswers}</AnswersCounter>
                <Description>ответов на вопросы</Description>
                <Container vertical='bottom' align='right'>
                    <LogoutButton onClick={logout}>Выйти</LogoutButton>
                </Container>
            </AnwsersContainer>
        </ModalComponent>
    )
}

const mapStateToProps = (state: IRootState) => ({
    university: getUniversity(state),
    idPrometeus: getIdPrometeus(state),

    profile: getProfile(state)
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
    logout: () => dispatch(commonActions.logout()),
})

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)