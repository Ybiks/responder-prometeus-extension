import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import RouterModal from './RouterModal';

export default (store) => {
    const html: HTMLElement = document.getElementsByTagName('html')[0];
    let container = document.createElement('div');
    
	html.appendChild(container);

	render(
        <>
            <Provider store={store}>
                <RouterModal />
            </Provider>
        </>,
		container
	);
};