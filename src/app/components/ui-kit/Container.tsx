import styled from 'styled-components';

interface IProps {
    align?: string;
    vertical?: string;
    padding?: string;
    children?: any;
}

export const Container = styled('div')<IProps>`
    text-align: ${ props => props.align || 'initial' };
    vertical-align: ${ props => props.vertical || 'initial' };
    padding: ${ props => props.padding || 'initial' }
`;
