import styled from 'styled-components';

interface IInputProps {
    error?: boolean;
}

export const Input = styled.input<IInputProps>`
    background-color: transparent;
    border-bottom: 1px solid ${ props => props.error ? 'red' : '#9e9e9'};
    border-top: none;
    border-right: none;
    border-left: none;
    border-radius: 0;
    outline: none;
    height: 25px;
    width: 100%;
    font-size: .8rem;
    margin: 0 0 12px 0;
    padding: 20px;
    &:disabled {
        color: #d0d0d0;
    }
`;

export const Warning = styled.p`
    margin-top: -12px;
    margin-bottom: 0;
    font-size: .7rem;
    color: red;
`;