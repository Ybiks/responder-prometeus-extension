import styled from 'styled-components';

export const Link = styled.a`
    color: #3c8dbc;
    font-size: .8rem;
`;