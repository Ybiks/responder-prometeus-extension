import styled from 'styled-components';

export const Button = styled.button`
    background-color: #3c8dbc;
    font-size: .8rem;
    border-radius: 0;
    border: none;
    color: #fff;
    cursor: pointer;
    transition: .3s;
    &:hover {
        opacity: .7;
    }
`;