import styled, { css } from 'styled-components';

interface IModalProps {
    visible: boolean;
}

export const Modal = styled.div<IModalProps>`
    right: 0;
    width: 330px;
    height: auto;
    position: fixed;
	z-index: 999999;
	text-align: center;
	background-color: #fff;
	font-family: "Source Sans Pro", sans-serif;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    transition: .3s;
    ${
        ({ visible }) => visible ? css`bottom: 0;` : css`bottom: -500px` 
    }
`;

export const ModalTitle = styled.p`
    background-color: #3c8dbc;
	font-size: 18px;
	color: #fff;
	margin: 0;
	padding: 0 10% 0 10%;
	text-align: left;
	line-height: 50px;
	height: 50px;
`;

export const ModalFullScreenButton = styled.img`
    margin-left: -17.75px;
	padding: 17.75px;
	cursor: pointer;
	float: left;
	width: 14.5px;
	height: 14.5px;
    transition: .3s;
    &:hover {
        opacity: .5;
    }
`;

export const ModalCloseButton = styled.img`
    margin-right: -18.75px;
	padding: 18.75px;
	cursor: pointer;
	float: right;
	width: 12.5px;
	height: 12.5px;
    transition: .3s;
    &:hover {
        opacity: .5;
    }
`;

export const ModalBody = styled.div`
    text-align: left;
	font-size: 16px;
`;