import { createStore, applyMiddleware, Store } from 'redux';
import createSagaMiddleware from 'redux-saga';

import reducers from './rootReducer';
import saga from './rootSaga';
import logger from '@/libs/logger';

const sagaMiddleware = createSagaMiddleware();
let middlewares = [sagaMiddleware];

if(process.env.NODE_ENV !== 'production'){
	middlewares = [...middlewares, require('redux-logger').default];
}

export default (initialState = {}): Store => {
    const store: any = createStore(reducers, initialState, applyMiddleware(...middlewares));

	sagaMiddleware.run(saga).done.catch(error => logger.sendCritical(error));
	return store;
};