import { ITokenState } from './modules/token/interface';
import { IProfileState } from './modules/profile/interface';
import { IIdPrometeusState } from './modules/id-prometeus/interface';

import { IModalState } from './modules/modal/interface';
import { ITargetsState } from './modules/targets/interface';
import { IUniversityState } from './modules/university/interface';
import { IAnswersCacheState } from './modules/answers-cache/interface';
import { IProtocolState } from './modules/protocol/interface';
import { IVerificationCode } from './modules/verification-code/interface';


export interface IRootState {
    user: {
        token: ITokenState;
        profile: IProfileState;
        idPrometeus: IIdPrometeusState;
    },
    app: {
        modal: IModalState;
        targets: ITargetsState;
        protocol: IProtocolState;
        university: IUniversityState;
        answersCache: IAnswersCacheState;
        verificationCode: IVerificationCode;
    },
}