import { createSelector } from 'reselect';

import { IRootState } from './interface';
import { IUserControl, UserControlStrategies } from '../interfaces/user';

const app = (state: IRootState) => state.app;
const user = (state: IRootState) => state.user;

// app
export const getTargets = createSelector(app, app => app.targets);
export const getProtocol = createSelector(app, app => app.protocol);
export const getUniversity = createSelector(app, app => app.university);
export const getAnswersChache = createSelector(app, app => app.answersCache);
export const getVerificationCode = createSelector(app, app => app.verificationCode);
export const getFramesFromTargets = createSelector(getTargets, targets => targets.map(target => target.frame));
export const getDocumentsFromTargets = createSelector(getTargets, targets => targets.map(target => target.document));

// app modal
export const getModal = createSelector(app, app => app.modal);
export const getIsShowModal = createSelector(getModal, modal => modal.show);

// user
export const getToken = createSelector(user, user => user.token);
export const getProfile = createSelector(user, user => user.profile);
export const getIdPrometeus = createSelector(user, user => user.idPrometeus)
export const getNumberOfAnswers = createSelector(getProfile, profile => profile.numberOfAnswers);

// user control

const defaultControl: IUserControl = {
    strategy: UserControlStrategies.manual,
    keysModal: [16, 72],
    showAnswer: [16, 74],
    keysHide: [16, 80],
    accuracy: 90,
}

export const getUserControl = createSelector(getProfile, profile => profile ? profile.settings.control : defaultControl);
export const getKeysShowAnswer = createSelector(getUserControl, control => control.showAnswer);
export const getKeysModal = createSelector(getUserControl, control => control.keysModal);
export const getStrategy = createSelector(getUserControl, control => control.strategy);
export const getKeysHide = createSelector(getUserControl, control => control.keysHide);