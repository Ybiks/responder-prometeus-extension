import { createAction } from 'redux-act';
import { IIdPrometeusState } from './interface';

export const SET_ID_PROMETEUS = `SET_ID_PROMETEUS`;

const setIdPrometeus = createAction<IIdPrometeusState>(SET_ID_PROMETEUS);

export const idPrometeusActions = {
	setIdPrometeus
};