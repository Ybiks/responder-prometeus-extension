import { createReducer } from 'redux-act';

import { idPrometeusActions } from './actions';
import { IIdPrometeusState } from './interface';

const initialState = (): IIdPrometeusState => null;

const reducer = createReducer({}, initialState());

reducer.on(idPrometeusActions.setIdPrometeus, (_, id) => id);

export default reducer;