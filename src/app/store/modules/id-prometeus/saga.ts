import { takeEvery } from 'redux-saga/effects';

import { idPrometeusActions, SET_ID_PROMETEUS } from '@/store/modules/id-prometeus/actions';
import logger from '@/libs/logger';

type LogSetIdPrometeus = ReturnType<typeof idPrometeusActions.setIdPrometeus>;
function* logSetIdPrometeus(action: LogSetIdPrometeus) {
    logger.setTagsMeta({ idPrometeus: action.payload });
}

function* idPrometeusWatcher(){
    yield takeEvery(SET_ID_PROMETEUS, logSetIdPrometeus);
}

export default idPrometeusWatcher;