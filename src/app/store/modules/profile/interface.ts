import { IUserPrivateData } from '../../../interfaces/user';

export interface IProfileState extends IUserPrivateData {}