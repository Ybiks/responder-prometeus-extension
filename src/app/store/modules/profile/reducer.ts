import { createReducer } from 'redux-act';

import { profileActions } from './actions';
import { IProfileState } from './interface';

const initialState = (): IProfileState => (null);

const reducer = createReducer({}, initialState());

reducer.on(profileActions.setProfile, (_, profileInfo) => profileInfo);

reducer.on(profileActions.dropProfile, initialState);

export default reducer;