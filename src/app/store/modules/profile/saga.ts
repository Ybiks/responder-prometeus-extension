import { takeEvery } from 'redux-saga/effects';

import { profileActions, SET_PROFILE, DROP_PROFILE } from '@/store/modules/profile/actions';
import logger from '@/libs/logger';

type LogSetProfile = ReturnType<typeof profileActions.setProfile>;
function* logSetProfile(action: LogSetProfile) {
    logger.setUserMeta({ profile: action.payload });
}

type LogDropProfile = ReturnType<typeof profileActions.dropProfile>;
function* logDropProfile(action: LogDropProfile) {
    logger.setUserMeta({ profile: action.payload });
}

function* profileWatcher(){
    yield takeEvery(SET_PROFILE, logSetProfile);
    yield takeEvery(DROP_PROFILE, logDropProfile);
}

export default profileWatcher;