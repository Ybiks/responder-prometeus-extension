import { createAction } from 'redux-act';

import { IProfileState } from './interface';

export const SET_PROFILE = 'SET_PROFILE';
export const UPDATE_NUMBER_OF_ANSWERS_FOR_PROFILE = 'UPDATE_NUMBER_OF_ANSWERS_FOR_PROFILE';
export const DROP_PROFILE = 'DROP_PROFILE';

const setProfile = createAction<IProfileState>(SET_PROFILE);
const dropProfile = createAction(DROP_PROFILE);

export const profileActions = {
    setProfile,
    dropProfile,
};
