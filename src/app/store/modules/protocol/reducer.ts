import { createReducer } from 'redux-act';

import { protocolActions } from './actions';
import { IProtocolState } from './interface';

const initialState = (): IProtocolState => null;

const reducer = createReducer({}, initialState());

reducer.on(protocolActions.setProtocol, (_, protocol) => protocol);

export default reducer;