import { createAction } from 'redux-act';

import { Protocol } from '@/interfaces/university';

export const SET_PROTOCOL = `SET_PROTOCOL`;

const setProtocol = createAction<Protocol>(SET_PROTOCOL);

export const protocolActions = {
	setProtocol,
};