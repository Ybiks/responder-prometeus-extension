import { createReducer } from 'redux-act';

import { targetsActions } from './actions';
import { ITargetsState } from './interface';

const initialState = (): ITargetsState => [];

const reducer = createReducer({}, initialState());

reducer.on(targetsActions.setTargets, (_, targets) => targets);

export default reducer;