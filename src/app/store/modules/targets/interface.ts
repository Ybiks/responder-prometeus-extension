export interface ICustomDocument extends Document {
    myCustomField?: boolean;
}

export interface ITarget {
    frame: HTMLFrameElement;
    document: ICustomDocument;
}

export type ITargetsState = ITarget[];