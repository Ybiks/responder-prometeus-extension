import { createAction } from 'redux-act';
import { ITargetsState } from './interface';

export const SET_TARGETS = `SET_TARGETS`;

const setTargets = createAction<ITargetsState>(SET_TARGETS);

export const targetsActions = {
	setTargets
};