import { IAnswerObject } from "@/api/interface";

export type IAnswersCacheState = {
    [key: string]: IAnswerObject[];
}