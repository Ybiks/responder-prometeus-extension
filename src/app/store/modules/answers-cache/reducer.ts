import { createReducer } from 'redux-act';

import { answersCacheActions } from './actions';
import { IAnswersCacheState } from './interface';

const initialState = (): IAnswersCacheState => ({});

const reducer = createReducer({}, initialState());

reducer.on(answersCacheActions.setAnswersToCache, (state, data) => ({
    ...state,
    [data.idQuestion]: data.answers
}));

export default reducer;