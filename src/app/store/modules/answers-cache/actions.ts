import { createAction } from 'redux-act';
import { IAnswerObject } from '@/api/interface';

export const SET_ANSWER_TO_CACHE = `SET_ANSWER_TO_CACHE`;

const setAnswersToCache = createAction<{ idQuestion: string, answers: IAnswerObject[] }>(SET_ANSWER_TO_CACHE);

export const answersCacheActions = {
	setAnswersToCache
};