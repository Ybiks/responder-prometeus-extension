import { createAction } from 'redux-act';
import {
    LOGOUT,
    INITIAL_APP,
    SIGN_IN_FAILURE,
    SIGN_IN, SIGN_IN_SUCCESS,
    EVENT_SET_ANSWER,
} from './constants';

const initialApp = createAction(INITIAL_APP);

const signIn = createAction(SIGN_IN);
const signInSuccess = createAction(SIGN_IN_SUCCESS);
const signInFailure = createAction(SIGN_IN_FAILURE);

const logout = createAction(LOGOUT);

const eventSetAnswer = createAction(EVENT_SET_ANSWER);

export const commonActions = {
    initialApp,

    signIn,
    signInSuccess,
    signInFailure,

    logout,

    eventSetAnswer,
};