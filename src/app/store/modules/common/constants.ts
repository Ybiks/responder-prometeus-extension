export const INITIAL_APP = 'INITIAL_APP';

export const SIGN_IN = 'SIGN_IN';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE';

export const LOGOUT = 'LOGOUT';

export const EVENT_SET_ANSWER = 'EVENT_SET_ANSWER';