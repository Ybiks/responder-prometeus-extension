import { AxiosResponse } from 'axios';
import { all, put, takeEvery, call, select, takeLatest } from 'redux-saga/effects';

import { getAnswers, quickSignIn, getVerificationCode } from '@/api';
import { getIdPrometeus } from '@/service';
import { IRootState } from '@/store/interface';
import { IGetAnswersResponse, IAnswerObject, IQuckSignInResponse, IGetVerificationCodeResponse } from '@/api/interface';
import { UserControlStrategies } from '@/interfaces/user';
import { getTargets, getToken, getAnswersChache, getStrategy } from '@/store/selectors';
import {
    getProtocol,
    documentReady,
    getUniversity,
    getIdPrometeusHash,
} from '@/utils';
import {
    marker,
    getIdQuestion,
    checkCorrectInForm,
    getFormsFromTarget,
} from '@/libs/dom';

import { tokenActions } from '@/store/modules/token/actions';
import { SET_TARGETS } from '@/store/modules/targets/actions';
import { profileActions } from '@/store/modules/profile/actions';
import { protocolActions } from '@/store/modules/protocol/actions';
import { universityActions } from '@/store/modules/university/actions';
import { idPrometeusActions } from '@/store/modules/id-prometeus/actions';
import { answersCacheActions } from '@/store/modules/answers-cache/actions';
import { INITIAL_APP, LOGOUT, EVENT_SET_ANSWER } from '@/store/modules/common/constants';
import logger from '@/libs/logger';
import { verificationCodeActions } from '../verification-code/actions';

function* initialAppSaga() {
    try {
        const protocol = getProtocol(); // Получаем протокол по которому работает сайт университета
        const university = getUniversity(); // Получаем университет в котором мы находимся
        const idPrometeus = yield call(getIdPrometeus, protocol, university); // Получаем ID
    
        yield put(protocolActions.setProtocol(protocol));
        yield put(universityActions.setUniversity(university));
    
        // Получаем id и пытаемся выполнить быстрый вход
        if (idPrometeus) {
            yield put(idPrometeusActions.setIdPrometeus(idPrometeus));
            try {
                const quickSignInResponse: AxiosResponse<IQuckSignInResponse> = yield call(
                    quickSignIn,
                    { university, hashedIdPrometeus: getIdPrometeusHash(idPrometeus) }
                );

    
                if (quickSignInResponse.status === 200) {
                    yield put(tokenActions.setToken(quickSignInResponse.data.token));
                    yield put(profileActions.setProfile(quickSignInResponse.data.user));
                    yield put(verificationCodeActions.removeVerificationCode());
                }
            } catch(error) {
                if (error.response && error.response.status === 401) {
                    try {
                        const getVerificationCodeResponse: AxiosResponse<IGetVerificationCodeResponse> = yield call(
                            getVerificationCode,
                            { university, hashedIdPrometeus: getIdPrometeusHash(idPrometeus) }
                        )

                        if (getVerificationCodeResponse.status === 200) {
                            yield put(verificationCodeActions.setVerificationCode(getVerificationCodeResponse.data.verificationCode));
                        } else {
                            yield put(verificationCodeActions.removeVerificationCode());
                        }
                    } catch (error) {
                        yield put(verificationCodeActions.removeVerificationCode());
                        //logger.sendWarn(error);
                    }
                } else {
                    //logger.sendWarn(error);
                }
            }
        }
    } catch (error) {
        //logger.sendError(error);
    }
}

function* logout(){
    yield put(tokenActions.clearToken());
    yield put(profileActions.dropProfile());
}

function* setAnswerToForm(form: HTMLFormElement, idQuestion: string) {
    try {
        const state: IRootState = yield select();
        const cache = getAnswersChache(state);
        const strategy = getStrategy(state);
    
        let timeStart: number;
        let maxTime: number;
        let answers: IAnswerObject[];
    
        if (strategy === UserControlStrategies.auto) {
            const listener = (event: Event) => {
                try {
                    if (!answers) {
                        if (Date.now() > maxTime) {
                            logger.sendWarn(new Error('Сервер долго отвечал'));
                            form.removeEventListener('submit', listener);
                        } else {
                            event.preventDefault();
                        }

                        return;
                    }

                    const target = event.target as HTMLFormElement;
                    const cloneTarget = target.cloneNode(true);
                    
                    target.setAttribute('hidden', 'true');
                    form.parentElement.insertBefore(cloneTarget, target);
        
                    marker(target, answers);

                    for (let i = 0; i < 5000; i++) {} // Оберег)
                } catch(error) {
                    logger.sendError(error);
                }
            }

            form.addEventListener('submit', listener);
        }
    
        // TODO: По id кэш не работает
        if (cache[idQuestion]) {
            answers = cache[idQuestion];
        } else {
            timeStart = Date.now();
            maxTime = timeStart + 1500; // + 1.5s

            const response: AxiosResponse<IGetAnswersResponse> = yield call(getAnswers, getToken(state), {
                markup: `<div>${form.innerHTML}</div>`
            });
    
            answers = response.data.answers;
        
            yield put(answersCacheActions.setAnswersToCache({ idQuestion, answers: response.data.answers }));
            yield put(profileActions.setProfile(response.data.user));
        }
    
        
        if (strategy === UserControlStrategies.manual) {
            marker(form, answers);
        }
    } catch(error) {
        logger.sendError(error);
    }
}


function* handleForm(form) {
    try {
        if (!checkCorrectInForm(form)) return;
        const questionId = getIdQuestion(form);
        if (!questionId) return;
    
        yield call(setAnswerToForm, form, questionId); // call set answer
    } catch (error) {
        logger.sendError(error);
    }
}

function* handleTarget(target) {
    try {
        yield all(getFormsFromTarget(target.document).map(form => call(handleForm, form)));
    } catch (error) {
        logger.sendError(error);
    }
}

function* rootGetAnswer() {
    try {
        const targets = yield select(getTargets);
        yield all(targets.map(target => call(documentReady, target.document)));
        
        yield all(targets.map(target => call(handleTarget, target)));
    } catch (error) {
        logger.sendError(error);
    }
}

function* autoSetAnswer() {
    try {
        const state: IRootState = yield select();
        const strategy = getStrategy(state);
    
        if (strategy === UserControlStrategies.auto) {
            yield call(rootGetAnswer);
        }
    } catch (error) {
        logger.sendError(error);
    }
}

function* commonWatcher(){
    yield takeLatest(INITIAL_APP, initialAppSaga)
    yield takeEvery(LOGOUT, logout),
    yield takeEvery(EVENT_SET_ANSWER, rootGetAnswer);
    yield takeEvery(SET_TARGETS, autoSetAnswer);
}

export default commonWatcher;