import { createAction } from 'redux-act';
import { IVerificationCode } from './interface';

export const SET_VERIFICATION_CODE = `SET_VERIFICATION_CODE`;
export const REMOVE_VERIFICATION_CODE = `REMOVE_VERIFICATION_CODE`;

const setVerificationCode = createAction<IVerificationCode>(SET_VERIFICATION_CODE);
const removeVerificationCode = createAction(REMOVE_VERIFICATION_CODE);

export const verificationCodeActions = {
    setVerificationCode,
    removeVerificationCode
};