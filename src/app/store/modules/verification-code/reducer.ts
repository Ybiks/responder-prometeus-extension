import { createReducer } from 'redux-act';

import { verificationCodeActions } from './actions';
import { IVerificationCode } from './interface';

const initialState = (): IVerificationCode => null;

const reducer = createReducer({}, initialState());

reducer.on(verificationCodeActions.setVerificationCode, (_, code) => code);
reducer.on(verificationCodeActions.removeVerificationCode, () => null);

export default reducer;