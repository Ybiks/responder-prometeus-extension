import { createAction } from 'redux-act';

export const SHOW_MODAL                = 'SHOW_MODAL';
export const HIDE_MODAL                = 'HIDE_MODAL';
export const AUTO_CHANGE_DISPLAY_MODAL = 'AUTO_CHANGE_DISPLAY_MODAL';

const showModal           = createAction(SHOW_MODAL);
const hideModal           = createAction(HIDE_MODAL);
const autoChangeDisplayModal = createAction(AUTO_CHANGE_DISPLAY_MODAL);

export const modalActions = {
    showModal,
    hideModal,
    autoChangeDisplayModal,
};