import { createReducer } from 'redux-act';

import { modalActions } from './actions';
import { IModalState } from './interface';

const initialState = (): IModalState => ({ show: false });

const reducer = createReducer({}, initialState());

reducer.on(modalActions.showModal, state => ({ ...state, show: true }));
reducer.on(modalActions.hideModal, state => ({ ...state, show: false }));
reducer.on(modalActions.autoChangeDisplayModal, state => ({ ...state, show: !state.show }));

export default reducer;