import { createAction } from 'redux-act';

import { IUniversityState } from './interface';

export const SET_UNIVERSITY = 'SET_UNIVERSITY';

const setUniversity = createAction<IUniversityState>(SET_UNIVERSITY);

export const universityActions = {
    setUniversity
}