import { takeEvery } from 'redux-saga/effects';

import { universityActions, SET_UNIVERSITY } from '@/store/modules/university/actions';
import logger from '@/libs/logger';

type LogSetUniversity = ReturnType<typeof universityActions.setUniversity>;
function* logSetUniversity(action: LogSetUniversity) {
    logger.setTagsMeta({ university: action.payload });
}

function* universityWatcher(){
    yield takeEvery(SET_UNIVERSITY, logSetUniversity);
}

export default universityWatcher;