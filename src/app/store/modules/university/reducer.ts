import { createReducer } from 'redux-act';

import { universityActions } from './actions';
import { IUniversityState } from './interface';

const initialState = (): IUniversityState => null;

const reducer = createReducer({}, initialState());

reducer.on(universityActions.setUniversity, (_, university) => university);

export default reducer;