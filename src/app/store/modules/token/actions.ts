import { createAction } from 'redux-act';

const REDUCER = 'TOKEN';
const NS = `${REDUCER}__`;

export const SET_TOKEN   = `${NS}SET`;
export const CLEAR_TOKEN = `${NS}CLEAR_TOKEN`;

const clearToken = createAction(CLEAR_TOKEN);
const setToken   = createAction<string>(SET_TOKEN);

export const tokenActions = {
	setToken,
	clearToken,
};