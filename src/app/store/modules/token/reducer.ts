import { createReducer } from 'redux-act';

import { tokenActions } from './actions';
import { ITokenState } from './interface';

const initialState = (): ITokenState => null;

const reducer = createReducer({}, initialState());

reducer.on(tokenActions.clearToken, () => null);
reducer.on(tokenActions.setToken, (_, token) => token);

export default reducer;