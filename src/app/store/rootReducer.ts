import { combineReducers } from 'redux';

// user
import token from './modules/token/reducer';
import profile from './modules/profile/reducer';
import idPrometeus from './modules/id-prometeus/reducer';

// app
import modal from './modules/modal/reducer';
import targets from './modules/targets/reducer';
import protocol from './modules/protocol/reducer';
import university from './modules/university/reducer';
import answersCache from './modules/answers-cache/reducer';
import verificationCode from './modules/verification-code/reducer';

export default combineReducers({
	user: combineReducers({
		token,
        profile,
        idPrometeus,
    }),
    app: combineReducers({
        modal,
        targets,
        protocol,
        university,
        answersCache,
        verificationCode
    })
});