import { all, fork } from 'redux-saga/effects';

import logger from '@/libs/logger';

import common from './modules/common/saga';
import profile from './modules/profile/saga';
import university from './modules/university/saga';
import idPrometeus from './modules/id-prometeus/saga';

export default function* rootSaga() {
    try {
        yield all([
            fork(common),
            fork(profile),
            fork(university),
            fork(idPrometeus),
        ]);
    } catch (error) {
        logger.sendCritical(error);
    }
}
