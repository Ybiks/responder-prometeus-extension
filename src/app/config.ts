const isProd = process.env.NODE_ENV == 'production';

export default {
    apiURL: isProd ? process.env.PRODUCTION_API : process.env.DEVELOPMENT_API,
    ravenURL: 'https://a62f7e5b1c6847778fc689ab3c3a587e@sentry.io/1370136'
};