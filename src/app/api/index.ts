import * as sha256 from 'sha256';
import axios, { AxiosPromise } from 'axios';

import config from '@/config';
import {
    IGetAnswersResponse,
    IQuckSignInResponse,
    IGetAnswersRequest,
    ISignInResponse,
    ISignInRequest,
    IQuckSignInRequest,
    IGetVerificationCodeRequest,
} from '@/api/interface';
import { ITokenState } from '@/store/modules/token/interface';

const getInstanceWithToken = (token: string) => axios.create({ headers: { 'Authorization': token } });

export const quickSignIn = (data: IQuckSignInRequest): AxiosPromise<IQuckSignInResponse> =>
    axios.post(`${config.apiURL}/user/sign-in/quick`, data);

export const signIn = ({ password, ...otherFields }: ISignInRequest): AxiosPromise<ISignInResponse> =>
    axios.post(`${config.apiURL}/user/sign-in`, {
        ...otherFields,
        password: sha256(password),
    });

export const getVerificationCode = (data: IGetVerificationCodeRequest) => 
    axios.post(`${config.apiURL}/user/profile/registration/verify/get`, data);

export const getAnswers = (token: ITokenState, data: IGetAnswersRequest): AxiosPromise<IGetAnswersResponse> =>
    getInstanceWithToken(token).post(`${config.apiURL}/question`, data);