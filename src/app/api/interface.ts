import { IUserPrivateData } from '@/interfaces/user';
import { University } from '@/interfaces/university';


export interface IQuckSignInRequest {
    university: University;
    hashedIdPrometeus: string;
}

export interface IQuckSignInResponse {
    token: string;
    user: IUserPrivateData;
}

export interface ISignInRequest {
    university: University;
    username: string;
    password: string;
}

export interface ISignInResponse {
    token: string;
    user: IUserPrivateData;
}

export interface IAnswerObject {
    type: string;
    question: string;
    variantsOfAnswers: string[];
    correctAnswers: string[];
}

export interface IGetAnswersRequest {
    markup: string;
}

export interface IGetAnswersResponse {
    user: IUserPrivateData;
    answers: IAnswerObject[];
}

export interface IGetVerificationCodeRequest {
    university: University;
    hashedIdPrometeus: string;
}

export interface IGetVerificationCodeResponse {
    verificationCode: string;
}