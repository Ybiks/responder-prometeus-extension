import 'es6-shim';
import * as documentReady from 'doc-ready';
import { bindActionCreators } from 'redux';

import initialStore from '@/store/store';
import modals from './components/modals';

import ConnectKeyboard from '@/libs/keyboard';

import { targetsActions } from '@/store/modules/targets/actions';
import { commonActions } from '@/store/modules/common/actions';
import { idPrometeusActions } from '@/store/modules/id-prometeus/actions';

import {
	initialWatcherForTargets,
} from '@/libs/dom';
import logger from '@/libs/logger';

const store = initialStore();
const actions = bindActionCreators({ ...targetsActions, ...idPrometeusActions }, store.dispatch);

logger.wrap({ level: 'critical' }, () => {
    documentReady(async () => {
        // Инициализация приложения
        store.dispatch(commonActions.initialApp());
    
        // Следит за изменениями страницы, и диспатчит новые таргеты.
        initialWatcherForTargets(actions.setTargets);
    
        // Изменение в состоянии приложения, приводит к переустановке обработчиков события клавиатуры на targets.
        store.subscribe(() => ConnectKeyboard(store));
        
        // Инициализация модального окна
        modals(store);
    });
});