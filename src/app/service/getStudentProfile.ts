import axios, { AxiosPromise } from 'axios';
import { Protocol, University } from '@/interfaces/university';

export default (protocol: Protocol, university: University): AxiosPromise<string> => {
	return axios.get(`${protocol}//${university}/close/students/viewProfile.asp`)
}
