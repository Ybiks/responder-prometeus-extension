import { University, Protocol } from '@/interfaces/university';

// import registerError from '../../api/registerError';
import getStudentProfile from './getStudentProfile';
import { sliceLastElement } from '@/utils';

export const getLoginInMarkupProfile = (markup: string): string =>
	sliceLastElement(
		markup.match(/User login[:=\s]*([a-z0-9]+)+/i)[1].match(/([a-z0-9]?)/gi)
	).join('');


export const getIdPrometeus = async (protocol: Protocol, university: University): Promise<string|null> => {
	try {
        const response = await getStudentProfile(protocol, university);
		return getLoginInMarkupProfile(response.data);
	} catch (error) {
		if(error.status == 401)
			return null;//Это не ошибка. Просто пользователь не залогинен.

		/* registerError({
			id: 0,
			error,
			userAgent: navigator.userAgent,
			location: window.location.href,
			message: 'Ошибка произошла в момент получения id Prometeus'
		}); */
		return null;
	}
};
