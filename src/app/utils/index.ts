import * as crypto from 'crypto';
import { University, Protocol } from "../interfaces/university";

export const sliceLastElement = arg => arg.slice(0, arg.length - 1);

export const processingText = text =>
	text.replace(/([\d]+\))|([\n\t])/gm, "").replace(/^([\s]+)|([\s]+)$/gm, "");

export const getUniversity = () => location.host as University;

export const getProtocol = () => location.protocol as Protocol;

export const documentReady = document => new Promise(resolve => {
	const interval = setInterval(() => {
		if(document.readyState === 'complete'){
			clearInterval(interval);
			resolve(true);
		}
	}, 150);
});

export const getIdPrometeusHash = (idPrometeus: string) => crypto.createHash('sha256').update(idPrometeus).digest('hex');