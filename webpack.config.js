const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const path = require('path');

const pathToApp = path.join(__dirname, 'src/app');

let plugins = [
	new Dotenv()
];

module.exports = {
	mode: process.env.NODE_ENV,
	context: __dirname,
	entry: {
		app: './src/app/index.ts',
		content: './src/content/index.ts'
	},
	output: {
		path: path.join(__dirname, './product/js'),
		filename: '[name].js'
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: 'awesome-typescript-loader',
			}
		]
	},
	resolve: {
		extensions: ['.js', '.jsx', '.tsx', '.ts'],
		alias: {
			'@': pathToApp,
		}
	},
	devtool: 'hidden-source-map',
	plugins,
	optimization: {
		minimizer: process.env.NODE_ENV == 'production' ? [new UglifyJsPlugin()] : [],
	},
};