[Repository](https://bitbucket.org/Ybiks/responder-prometeus-extension/src/master/)

### Start project
1. Install dependency ```npm install```
2. Copy .env file, using command cp ```.env.example .env```
3. Start project, using command ```npm run dev```
4. Include extension folder (extension) to your browser
5. Go to the [website](http://cdo.bukep.ru) in the browser. And login in system using: login - ```1114081``` / password - ```1234568```  
6. Open extension ```Shift + H```
7. Use the same password ```1234568```
8. Hide extension
9. Try to pass any test in the system
